**To get footprints from JLCPCB part\# to use in KiCad:**
- Ensure you have Pyton v3.6 and above
- Run the following command
_pip install JLC2KiCadLib_
- Ensure that you have installed numpy
- Then navigate to the folder you want to place your footprints and symbols in
- Open terminal in that folder
- Run command:
_JLC2KiCadLib C6482_
Where C6482 is JLCPCB part number
