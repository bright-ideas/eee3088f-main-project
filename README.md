# EEE3088F Main Project

## Project Status
- PCB design complete. Currently awaiting production and shipping from China (JLCPCB)
- pcb-no-tracks branch: this branch is a backup branch of main, when it is confirmed that there is no corruption anywhere in the main branch, the pcb-no-tracks branch can be deleted. 

## Issues
Power subsystem: 
- When ordering the board, D5 and D6 were not populated since they had the wrong packages. This is not much of a problem since the Schottky diodes are easy to solder on (through hole components)

## Name
Ambient Light & Temperature Operation (A.L.T.O)

## Group members
Thuto Luphahla (Sensing)
Thiyashan Pillay (Power Management)
Dante' Webber (Microcontroller interfacing + Coding)

## Description
Our device uses a light sensor(analogue) and temperature sensor(digital) to measure and adjust the environment. The light sensing device will be able to detect the amount of natural sunlight received in a room and use this information to adjust the blinds in the room. The temperature sensor will detect when user defined temperature thresholds are breached in the room and adjust the blinds to keep the room within the desired temperature range. The blinds/curtains will be adjusted by a motor controlled by the device to adjust both the lighting and the temperature in the room. Information on the amount of light desired and temperature in the room can be pre-set by the user on the device. There will be a feature that will allow the user to set a time when the blinds should automatically shut for the evening and open again in the morning. 
All rooms are different. The light and temperature in any room at a given time is always changing and needs constant changing. Our device will hopefully achieve this.

## Usage
A person who works from home who wishes for their office to have the perfect lighting throughout the day. They can install the device and have it preset to only work during their office hours. When the room is not in use the device will keep the blinds closed. 

